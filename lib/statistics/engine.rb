module Statistics
  class Engine < ::Rails::Engine
    isolate_namespace Statistics

    initializer 'statistics.action_controller' do |app|
      ActiveSupport.on_load :action_controller do
        helper Statistics::ApplicationHelper
      end
    end
  end
end
