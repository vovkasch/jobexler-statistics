Statistics::Engine.routes.draw do
  resources :candidates, only: :index
  resources :employers, only: :index
end
