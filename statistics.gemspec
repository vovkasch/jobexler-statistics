$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "statistics/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "statistics"
  s.version     = Statistics::VERSION
  s.authors     = ["Volodymyr Shcherbyna"]
  s.email       = ["scherbina.v@gmail.com"]
  s.homepage    = "http://jobexler.net"
  s.summary     = "Performs statistics for jobexler.net."
  # s.description = "Performs statistics for jobexler.net."

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.0.0"
end
