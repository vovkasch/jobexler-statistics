module Statistics
  class Employer < User
    default_scope { includes(:completed_interviews).where(role: 'employer') }

    has_many :completed_interviews, 
              -> { merge(Interview.completed) },
              class_name: 'Interview', 
              inverse_of: :applicant, 
              foreign_key: :applicant_id

    def completed_interviews_amount
      completed_interviews.size
    end
  end
end
