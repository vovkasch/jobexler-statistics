# require_dependency "statistics/application_controller"

module Statistics
  class EmployersController < ApplicationController
    layout 'dashboard'

    load_and_authorize_resource class: 'Statistics::Employer'

    before_action :set_employer, only: [:show, :edit, :update, :destroy]

    # GET /employers
    def index
      @employers = Employer.includes(:completed_interviews)
                           # .references(:interviews)
                           # .group('users.id', 'interviews.id', 'completed_interviews_users.id')
    end

    # GET /employers/1
    def show
    end

    # GET /employers/new
    def new
      @employer = Employer.new
    end

    # GET /employers/1/edit
    def edit
    end

    # POST /employers
    def create
      @employer = Employer.new(employer_params)

      if @employer.save
        redirect_to @employer, notice: 'Employer was successfully created.'
      else
        render action: 'new'
      end
    end

    # PATCH/PUT /employers/1
    def update
      if @employer.update(employer_params)
        redirect_to @employer, notice: 'Employer was successfully updated.'
      else
        render action: 'edit'
      end
    end

    # DELETE /employers/1
    def destroy
      @employer.destroy
      redirect_to employers_url, notice: 'Employer was successfully destroyed.'
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_employer
        @employer = Employer.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def employer_params
        params[:employer]
      end
  end
end
