# require_dependency "statistics/application_controller"

module Statistics
  class CandidatesController < ApplicationController
    layout 'dashboard'

    load_and_authorize_resource class: 'Statistics::Candidate'

    before_action :set_candidate, only: [:show, :edit, :update, :destroy]

    # GET /candidates
    def index
      @candidates = Candidate.all
    end

    # GET /candidates/1
    def show
    end

    # GET /candidates/new
    def new
      @candidate = Candidate.new
    end

    # GET /candidates/1/edit
    def edit
    end

    # POST /candidates
    def create
      @candidate = Candidate.new(candidate_params)

      if @candidate.save
        redirect_to @candidate, notice: 'Candidate was successfully created.'
      else
        render action: 'new'
      end
    end

    # PATCH/PUT /candidates/1
    def update
      if @candidate.update(candidate_params)
        redirect_to @candidate, notice: 'Candidate was successfully updated.'
      else
        render action: 'edit'
      end
    end

    # DELETE /candidates/1
    def destroy
      @candidate.destroy
      redirect_to candidates_url, notice: 'Candidate was successfully destroyed.'
    end

    private
      # Use callbacks to share common setup or constraints between actions.
      def set_candidate
        @candidate = Candidate.find(params[:id])
      end

      # Only allow a trusted parameter "white list" through.
      def candidate_params
        params[:candidate]
      end
  end
end
